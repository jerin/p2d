#include <glm/glm.hpp>
#include <vector>

/* 
 * The screen is scaled to 16:9 coordinate system with
 * the below macros defining the boundaries.
 */
#define LEFT_END 0 
#define RIGHT_END 320
#define TOP_END 180
#define BOTTOM_END 0

#define TURN_SPEED 0.05
#define ZOOM_DELTA 0.05

#define GROUND_OFFSET 10
#define LEFT_OFFSET 10
#define RIGHT_OFFSET 10

#define MIN_SPEED 30.0
#define MAX_SPEED 70.0
#define DELTA_SPEED 3.0

#define MAX_BW 10
#define MAX_OW 30
#define MAX_BH 100

#define EPS 0.01

#define BASE_RADIUS 5.0
#define CANNON_WIDTH 30.0
#define CANNON_HEIGHT 5.0

#define _RE 0.8

namespace colors{
	std::vector<float> red = {1.0f, 0.0f, 0.0f};
	std::vector<float> green = {0.0f, 1.0f, 0.0f};
	std::vector<float> blue = {0.0f, 0.0f, 1.0f};
	
	std::vector<float> white = {1.0f, 1.0f, 1.0f};
	std::vector<float> black = {0.0f, 0.0f, 0.0f};
	
	std::vector<float> brown = {0.3f, 0.0f, 0.0f};
	std::vector<float> orange = {0.6f, 0.3f, 0.0f};
	std::vector<float> grass = {0.0f, 0.3f, 0.0f};
}

struct GLMatrices {
    glm::mat4 projection;
    glm::mat4 model;
    glm::mat4 view;
    GLuint MatrixID;
} Matrices;


