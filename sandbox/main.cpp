#include <iostream>
#include <cmath>
#include <fstream>
#include <vector>
#include <list>
#include <GL/glew.h>
#include <GL/glu.h>
#include <cstdlib>
#include <ctime>
#include <GL/freeglut.h>
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "shader.hpp"
#include "definitions.hpp"
#include "object.hpp"
using namespace std;

GLuint programID;
float factor = M_PI/4;
int eyepos = 55;
float launch_speed = 70;
float zf = 1;
glm::mat4 overground(1.0f), VP(1.0f);
glm::vec3 cannonTip;

struct score_t{
    int shots;
    int kills;
    score_t():shots(0), kills(0){};
}score;

circle base, coin, pig;
rectangle ground, obstacle, cannon;

list<bird> birds;
list<rectangle> obstacles;
list<circle> coins, pigs;

float currentTime(){
	float f = glutGet(GLUT_ELAPSED_TIME)/500.0;
	return f;
}

void launchBird(){
    score.shots += 1;
	bird b = bird(3.0f, cannonTip.x, cannonTip.y, colors::black);
	b.launch(launch_speed*cos(factor), launch_speed*sin(factor), currentTime()) ;
	birds.push_back(b);
}


void keyboardDown (unsigned char key, int x, int y){
	switch (key) {
		case 'Q':
		case 'q':
		case 27: 
			exit (0);
		case 'a':
		case 'A':
			factor = min(M_PI/2, factor+TURN_SPEED);
			break;
		case 'b':
		case 'B':
			factor = max(0.0, factor-TURN_SPEED);
			break;		
		case 'f':
		case 'F':
			launch_speed = min(MAX_SPEED, launch_speed + DELTA_SPEED);
			break;
		case 's':
		case 'S':
			launch_speed = max(MIN_SPEED, launch_speed - DELTA_SPEED);
			break;
		case ' ':			  
			launchBird();
			break;		
		default:
			break;
	}
}


void keyboardUp (unsigned char key, int x, int y){
	switch (key) {
		case 'c':
		case 'C':
			break;
		case 'p':
		case 'P':
			break;
		case 'x':

			break;
		default:
			break;
	}
}

void keyboardSpecialDown (int key, int x, int y){
	switch(key){
		case GLUT_KEY_UP:
			zf = max(0.3, zf - ZOOM_DELTA);
			break;
		case GLUT_KEY_DOWN:
			zf = min(1.0, zf + ZOOM_DELTA);
			break;
		default: break;
	}
}

void keyboardSpecialUp (int key, int x, int y){

}

void mouseClick (int button, int state, int x, int y){
	switch (button){
		case GLUT_LEFT_BUTTON:
			if (state == GLUT_UP)
				launchBird();
			break;
		case GLUT_RIGHT_BUTTON:
			if (state == GLUT_UP) {
			}
			break;
		default:
			break;
	}
}

void mouseMotion (int x, int y) {
}

void passiveMouseMotion(int x, int y){
	float xf = x, yf = -y + 900;
    xf -= cannonTip.x, yf -= cannonTip.y;
	float newFactor = atan2(yf,xf), r = sqrt(xf*xf + yf*yf);
    float multiplier = 0.1;
	factor = max(0.0f, newFactor);
	factor = min((float)M_PI/2, factor);
    launch_speed = min(multiplier*r, (float)MAX_SPEED);
    launch_speed = max(launch_speed, (float)MIN_SPEED);
}

void reshapeWindow (int width, int height) {
	glViewport (0, 0, (GLsizei) width, (GLsizei) height);
	Matrices.projection = glm::ortho((float)LEFT_END, 
			(float)RIGHT_END, (float)BOTTOM_END, 
			(float)TOP_END, 0.1f, 500.0f);
}

void drawObject(object &ob){
	glm::mat4 MVP(1.0f);
	MVP = VP*ob.model;
	glUniformMatrix4fv(Matrices.MatrixID, 1, GL_FALSE, &MVP[0][0]);
	draw3DObject(ob.v);
}

void draw () {
	glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram (programID);
	glm::vec3 eye(0, 0, eyepos), target(0, 0, 0), up(0, 1, 0);
	Matrices.projection = glm::ortho(zf*(float)LEFT_END, 
			zf*(float)RIGHT_END, zf*(float)BOTTOM_END, 
			zf*(float)TOP_END, 0.1f, 500.0f);

	Matrices.view = glm::lookAt(eye, target, up); 

	glm::mat4 T, S, R;
	VP = Matrices.projection * Matrices.view;    

	drawObject(base);

	R = glm::rotate(factor, glm::vec3(0, 0, 1));
	cannon.model = base.model * R * glm::translate(glm::vec3(0, -CANNON_HEIGHT/2.0f, 0));        
	drawObject(cannon);
	cannonTip = glm::vec3(cannon.model * glm::vec4(CANNON_WIDTH, CANNON_HEIGHT/2.0f, 0, 1));
	//cannonTip.x += LEFT_OFFSET, cannonTip.y += GROUND_OFFSET;

	drawObject(ground); 

	list<bird> ::iterator p;
	list<rectangle>::iterator q;
	float t;
	for(p = birds.begin(); p!=birds.end();){		
		t = currentTime();
		p->model = ground.model*(glm::translate(p->position(t))); //Updates position.
		glm::vec3 v = p->velocity(t); //Update velocity
		if(p->x + p->r < 0 or p->x- p->r > RIGHT_END or 
                (v.x < EPS and v.y < EPS )){
			p = birds.erase(p);
		}
		else if( p->y - p->r < GROUND_OFFSET){
			p->xi = p->x; 
            p->yi = max(p->y, GROUND_OFFSET+p->r);
			p->uy = -0.8*v.y, p->ux = v.x;
			p->ti = currentTime();
			if ( p->uy - 0 < EPS ){
				p->friction = 1.0;
				p->uy = 0;
			}
		}
        
		else{
			drawObject(*p);	
			p++;
		}

	}

	for(q = obstacles.begin(); q!=obstacles.end(); q++){
		drawObject(*q);
	}

    list<circle>::iterator c;
    for(c = pigs.begin(); c!=pigs.end(); c++){
        drawObject(*c);
    }

    for(q = obstacles.begin(); q!=obstacles.end(); q++){
        for(p =  birds.begin(); p!= birds.end();){
            if ( collides(*p, *q)){
                if(isCollisionX(*p, *q)){
                    p->xi = p->x; 
                    p->yi = p->y + EPS*10;
                    p->ti = currentTime();
                    p->ux = p->vx;
                    p->uy = -_RE*p->vy;
                }
                else{
                    p->xi = p->x; 
                    p->yi = p->y; 
                    p->ti = currentTime();
                    p->uy = p->vy;
                    p->ux = -_RE*p->vx;
                    if ( p->ux <= 0)
                        p->x -= EPS*100;
                    else
                        p->x += EPS*100;
                    
                };
                p++;
            }
            else p++;
		}
	}


    for(c = pigs.begin(); c!=pigs.end();){
        bool hit = false;
        for(p = birds.begin(); p!=birds.end(); ){
            if(collides(*p, *c)){
                p = birds.erase(p);
                c = pigs.erase(c);
                score.kills += 1;
                hit = true;
            }
            else{
                p++;
            }
        }
        if ( not hit ) c++;
    }
	glutSwapBuffers();

}

void idle () {
	draw (); 
}

void initGLUT (int& argc, char** argv, int width, int height) {
	glutInit (&argc, argv);

	glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitContextVersion (3, 3); 
	glutInitContextFlags (GLUT_CORE_PROFILE); 
	glutInitWindowSize (width, height);
	glutCreateWindow ("P2D");

	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	if (err != GLEW_OK) {
		cout << "Error: Failed to initialise GLEW : "<< glewGetErrorString(err) << endl;
		exit (1);
	}

	glutKeyboardFunc (keyboardDown);
	glutKeyboardUpFunc (keyboardUp);
	glutSpecialFunc (keyboardSpecialDown);
	glutSpecialUpFunc (keyboardSpecialUp);
	glutMouseFunc (mouseClick);
	glutMotionFunc (mouseMotion);
	glutPassiveMotionFunc(passiveMouseMotion);
	glutReshapeFunc (reshapeWindow);
	glutDisplayFunc (draw); 
	glutIdleFunc (idle); 
	glutIgnoreKeyRepeat (false); 
}

void menu(int op) {
	switch(op)
	{
		case 'Q':
		case 'q':
			exit(0);
	}
}

void addGLUTMenus () {
	int subMenu = glutCreateMenu (menu);
	glutAddMenuEntry ("Do Nothing", 0);
	glutAddMenuEntry ("Really Quit", 'q');

	glutCreateMenu (menu);
	glutAddSubMenu ("Sub Menu", subMenu);
	glutAddMenuEntry ("Quit", 'q');
	glutAttachMenu (GLUT_MIDDLE_BUTTON);
}

void initRealm(){

	//Initialize models of static objects here.
	overground = glm::translate(glm::vec3(LEFT_END, BOTTOM_END+GROUND_OFFSET, 0.0f));

	ground = rectangle(2*RIGHT_END, GROUND_OFFSET, colors::grass);
	cannon = rectangle(CANNON_WIDTH, CANNON_HEIGHT, colors::brown);  

	//Model cannon's base. Place it.
	base = circle(BASE_RADIUS, LEFT_END+LEFT_OFFSET, RIGHT_END+RIGHT_OFFSET, colors::brown);
	base.model = glm::translate (glm::vec3(LEFT_END+LEFT_OFFSET, BOTTOM_END+GROUND_OFFSET, 0.0f));   

    float br, bh, ow;
    for(float x = LEFT_OFFSET+60.0f, inc_factor = 0;  x < RIGHT_END - 50.0f; x += inc_factor){
        br = 5.0+rand()%MAX_BW; bh = rand()%(int)floor(MAX_BH*(x/RIGHT_END)); ow = 5.0+rand()%MAX_OW;
        if(rand()%2){
            pig = circle(br, x + 0.5*ow, GROUND_OFFSET+bh + br, colors::green);
            pig.model = overground*glm::translate(glm::vec3(x+0.5*ow, bh + br, 0.0f));
            pigs.push_back(pig);
        }

        obstacle = rectangle(ow, bh, colors::orange);
        obstacle.model = overground*glm::translate(glm::vec3(x, 0, 0.0f));
        obstacle.updateBC();
        obstacles.push_back(obstacle);

        inc_factor = max(2*br, ow);

    }
    /*
	//Obstacles, ah, hardcodes.
	obstacle = rectangle(20.0f, 30.0f, colors::orange);
	obstacle.model = overground*glm::translate(glm::vec3(RIGHT_END - RIGHT_OFFSET- 20.0f, 0, 0.0f));
    obstacle.updateBC();
	obstacles.push_back(obstacle);

	obstacle = rectangle(30.0f, 40.0f, colors::orange);
	obstacle.model = overground*glm::translate(glm::vec3(RIGHT_END - RIGHT_OFFSET - 90.0f, 0, 0.0f));
    obstacle.updateBC();
	obstacles.push_back(obstacle);

	obstacle = rectangle(60.0f, 10.0f, colors::orange);
	obstacle.model = overground*glm::translate(glm::vec3(RIGHT_END - RIGHT_OFFSET - 150.0f, 0, 0.0f));
    obstacle.updateBC();
	obstacles.push_back(obstacle);

    pig = circle(6.0f, RIGHT_END-RIGHT_OFFSET - 120.0f, GROUND_OFFSET + 16.0f, colors::green);
	pig.model = overground*glm::translate(glm::vec3(RIGHT_END - RIGHT_OFFSET- 120.0f, 16.0f, 0.0f));
    pigs.push_back(pig);

    pig = circle(10.0f, RIGHT_END-RIGHT_OFFSET - 50.0f, GROUND_OFFSET + 10.0f, colors::green);
	pig.model = overground*glm::translate(glm::vec3(RIGHT_END - RIGHT_OFFSET- 50.0f, 10.0f, 0.0f));
    pigs.push_back(pig);
    
    pig = circle(10.0f, RIGHT_END-RIGHT_OFFSET - 250.0f, GROUND_OFFSET + 10.0f, colors::green);
	pig.model = overground*glm::translate(glm::vec3(RIGHT_END - RIGHT_OFFSET- 250.0f, 10.0f, 0.0f));
    pigs.push_back(pig);
    
    pig = circle(6.0f, RIGHT_END-RIGHT_OFFSET - 250.0f, GROUND_OFFSET + 19.0f, colors::green);
	pig.model = overground*glm::translate(glm::vec3(RIGHT_END - RIGHT_OFFSET- 250.0f, 19.0f, 0.0f));
    pigs.push_back(pig);

    for(int i=0; i<rand()%10; i++){
        float hr, wr;
        hr = rand()%100, wr = rand()%240;
        pig = circle(5.0f, LEFT_OFFSET + wr, GROUND_OFFSET + hr, colors::green);
        pig.model = overground*glm::translate(glm::vec3(LEFT_OFFSET+wr, hr, 0.0f));
        pigs.push_back(pig);
    }
    */
}

void initGL (int width, int height) {
	programID = LoadShaders( "Sample_GL.vert", "Sample_GL.frag" );
	Matrices.MatrixID = glGetUniformLocation(programID, "MVP");
	reshapeWindow (width, height);
	vector<float> color;
	glClearColor (0.3f, 0.3f, 1.0f, 0.0f); 
	glClearDepth (1.0f);
	glEnable (GL_DEPTH_TEST);
	glDepthFunc (GL_LEQUAL);    

	initRealm(); //Draws the realm.
}

int main (int argc, char** argv){
	srand(time(NULL));
	int width = 1600;
	int height = 900;
	initGLUT (argc, argv, width, height);
	addGLUTMenus ();
	initGL (width, height);
	glutMainLoop ();
	return 0;
}
