#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <vector>
#include "vao.hpp"

using namespace std;

class mesh{
    struct vertex{
        float x, y, z;
    };

    float xo, yo, r; //Bounding circle test.
    vector<vertex> V, C;

    inline void add_vertex(float x, float y, float z){
        vertex v(x, y, z);
        V.push_back(v);
    }

    inline compute_bounding_circle(){
        //Centroid is center. 
        //Average of distances from centroid is the radius.
    }

    inline void add_color(float x, float y, float z){
        vertex v(x, y, z);
        C.push_back(v);
    }

    struct VAO* draw(){
        return create3DObject(GL_TRIANGLES, 3, vertex_buffer, color_buffer, GL_FILL);
    }
};
