#include "object.hpp"
#include <glm/gtx/transform.hpp>
#include <glm/gtc/matrix_transform.hpp>

#define EPS 0.01

void bird::launch(float uxx, float uyy, float t){
    ti = t; ux = uxx, uy = uyy;
}

glm::vec3 bird::position(float ct){
    float t = ct - ti;
    float dx, dy;
    dx = (ux/_ALPHA)*(1-exp(-_ALPHA*t)) + friction*(_MU*_G/_ALPHA)*(t - (1/_ALPHA)*(1 - exp(-_ALPHA*t)));
    dy = (uy/_ALPHA)*(1-exp(-_ALPHA*t)) - (_G/_ALPHA)*(t - (1/_ALPHA)*(1 - exp(-_ALPHA*t)));
    x = xi + dx, y = yi + dy; 
    cx = x, cy = y;
    return glm::vec3(x, y, 0);
};


glm::vec3 bird::velocity(float ct){
    float t = ct - ti; 
    vx = ux*(exp(-_ALPHA*t));
    vy = uy*(exp(-_ALPHA*t)) - (_G/_ALPHA)*(1-exp(-_ALPHA*t));
    return glm::vec3(vx, vy, 0);
};

float distance(float px, float py, float qx, float qy){
	float dx = px - qx, dy = py - qy;
	return sqrt(dx*dx + dy*dy);
}


template<class A, class B>
bool bounding(A &b,  B &r){
    float c1c2, sr;
    c1c2 = distance(b.cx, b.cy, r.cx, r.cy);
    sr = b.r + r.r;
    //fprintf(stderr, "c1 = (%f, %f), c2 = (%f,%f)\n", b.cx, b.cy, r.cx, r.cy);
    //fprintf(stderr, "c1c2 = %f, r1+r2 = %f\n", c1c2,sr);
    return c1c2 <= sr;
}

bool collides(bird &b, circle &c){
    if( bounding(b, c)){
        return true;
    }
    return false;
}



bool collides(bird &b,  rectangle &r){
    if(not bounding(b, r))
        return false;
	glm::vec2 center(b.cx, b.cy);
	// Calculate AABB info (center, half-extents)
    //fprintf(stderr, "Bird center(%f, %f)\n", b.cx, b.cy);
	glm::vec2 aabb_half_extents(r.lx/2.0, r.by/2.0);
    //fprintf(stderr, "Half extends(%f, %f)\n", r.lx/2.0, b.cy);
	glm::vec2 aabb_center(r.cx, r.cy);
    //fprintf(stderr, "aabb center(%f, %f)\n", r.cx, r.cy);
	// Get difference vector between both centers
	glm::vec2 difference = center - aabb_center;
	glm::vec2 clamped = glm::clamp(difference, -aabb_half_extents, aabb_half_extents);
	// Add clamped value to AABB_center and we get the value of box closest to circle
	glm::vec2 closest = aabb_center + clamped;
	// Retrieve vector between center circle and closest point AABB and check if length <= radius
	difference = closest - center;
	return glm::length(difference) < b.r;
}


bool isCollisionX(bird &b, rectangle &r){
	glm::vec2 center(b.cx, b.cy);
	glm::vec2 aabb_half_extents(r.lx/2.0, r.by/2.0);
	glm::vec2 aabb_center(r.cx, r.cy);
	glm::vec2 difference = center - aabb_center;
	glm::vec2 clamped = glm::clamp(difference, -aabb_half_extents, aabb_half_extents);
	glm::vec2 closest = aabb_center + clamped;
    if ( abs(closest.y -(aabb_center.y + aabb_half_extents.y))<EPS)
        return true;
    return false;
}
