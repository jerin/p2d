#include <cmath>
#include "types.h"

float distance(const V2 &a, const V2 &b){
    float dx, dy;
    dx = a.x - b.x, dy = a.y - b.y;
    return sqrt(dx*dx + dy*dy);
}

bool collides(const sprite &a, const sprite &b){
    //Simple bounding circle check. c1c2 <= r1 + r2 implies intersection
    return distance(a.p, b.p) - (a.r + b.r) <= EPS;
}
