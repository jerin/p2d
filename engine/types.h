
#define EPS 1e-5

struct V2{
    float x, y;
};

struct sprite{
    V2 p;
    float r;
};


float distance(const V2 &, const V2 &);
bool collides(const sprite &, const sprite &);

