#include "object.hpp"
#include <glm/gtx/transform.hpp>
#include <glm/gtc/matrix_transform.hpp>

#define EPS 0.01

void bird::launch(float uxx, float uyy, float t){
    ti = t; ux = uxx, uy = uyy;
}

glm::vec3 bird::position(float ct){
    float t = ct - ti;
    float dx, dy;
    dx = (ux/_ALPHA)*(1-exp(-_ALPHA*t)) + friction*(_MU*_G/_ALPHA)*(t - (1/_ALPHA)*(1 - exp(-_ALPHA*t)));
    dy = (uy/_ALPHA)*(1-exp(-_ALPHA*t)) - (_G/_ALPHA)*(t - (1/_ALPHA)*(1 - exp(-_ALPHA*t)));
    x = xi + dx, y = yi + dy; 
    return glm::vec3(x, y, 0);
};


glm::vec3 bird::velocity(float ct){
    float t = ct - ti; 
    vx = ux*(exp(-_ALPHA*t));
    vy = uy*(exp(-_ALPHA*t)) - (_G/_ALPHA)*(1-exp(-_ALPHA*t));
    return glm::vec3(vx, vy, 0);
};

float distance(float px, float py, float qx, float qy){
	float dx = px - qx, dy = py - qy;
	return sqrt(dx*dx + dy*dy);
}
