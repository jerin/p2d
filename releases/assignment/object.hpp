/* 
 * Every objects' associated model. 
 * The model is required to compute world coordinates
 * Useful when it comes to detecting collisions.
 * 
 */

#include <glm/glm.hpp>
#include "vao.hpp"
#include <cmath>

#define DELTA_TIME 0.05
#define _G 9.81
#define _MU 0.7
#define _ALPHA 0.1

struct object{
	glm::mat4 model; //Mutiplying (x, y) with model gives the world coordinates.
	VAO *v; //VAO associated with the object
    float cx, cy, r; 
    object():cx(0), cy(0){}
};

struct circle:public object{
    circle(){};
    circle(float radius, float x, float y, vector<float> color){ 
        cx = x, cy = y, r = radius; //Set basic values.
        //Compute VAO object
        vector<GLfloat> VB, CB;
        float twoPI = 2*M_PI, arg;
        for(int i=0; i<_CDIVS; i++){
            VB.insert(VB.end(), {0.0f, 0.0f, 0.0f});
            arg = i*twoPI/_CDIVS;
            VB.insert(VB.end(), {r*cos(arg), r*sin(arg), 0.0f});
            arg = (i+1)*twoPI/_CDIVS;
            VB.insert(VB.end(), {r*cos(arg), r*sin(arg), 0.0f});
        }
        for(int i=0; i<3*_CDIVS; i++){
            CB.insert(CB.end(), color.begin(), color.end());
        }
        v = fromVC(VB, CB);
    }
};

struct rectangle:public object{
    rectangle(){};
    rectangle(float lx, float by, vector<float> color){
        cx = lx/2, cy = by/2, r = 0.5*sqrt(lx*lx + by*by);
        vector<GLfloat> VB, CB;
        for(int i=0; i<2; i++){
            VB.insert(VB.end(), {0, 0, 0});
            VB.insert(VB.end(), {i*lx, (!i)*by, 0});
            VB.insert(VB.end(), {lx, by, 0});
        }
        for(int i=0; i<6; i++){
            CB.insert(CB.end(), color.begin(), color.end());
        }	
        v =  fromVC(VB, CB);
    }
};

struct bird: public circle {
	float x, y, xi, yi, ti;
    float ux, uy, vx, vy;
    float friction;
    bird(float radius, float xx, float yy, vector<float> color): 
        circle(radius, xx, yy, color), xi(xx), yi(yy), friction(0.0){
            assert(xi > 0 && yi > 0);
    };

    void launch(float uxx, float uyy, float);
    glm::vec3 velocity(float);
    glm::vec3 position(float);
};


float distance(float, float, float, float);
