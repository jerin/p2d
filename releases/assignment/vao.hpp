
#include <GL/glew.h>
#include <iostream>
#include <cmath>
#include <vector>
using namespace std;

/* Circle Divisions, increase to get smoother circle.*/
#define _CDIVS 30

struct VAO {
    GLuint VertexArrayID;
    GLuint VertexBuffer;
    GLuint ColorBuffer;

    GLenum PrimitiveMode;
    GLenum FillMode;
    int NumVertices;
};


VAO* create3DObject (GLenum, int, const GLfloat*, const GLfloat*, GLenum);
VAO* create3DObject (GLenum, int numVertices, const GLfloat* , const GLfloat, const GLfloat, const GLfloat, GLenum);
void draw3DObject (struct VAO* vao);
VAO *fromVC(vector<GLfloat> &VB, vector<GLfloat> &CB);
